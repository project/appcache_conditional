<?php

/**
 * @file
 * Administrative page callbacks.
 */

function appcache_conditional_settings_form($form_state) {
  $form = array();

  $form['appcache_conditional_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the HTML5 offline application cache'),
    '#default_value' => variable_get('appcache_conditional_enabled', FALSE),
  );

  $form['appcache_conditional_automatic'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically cache visited pages and resources'),
    '#description' => t('This setting will tell browsers to cache all resources they visit, except for any Administrative pages. In this mode, no other modules will be able to modify the application cache manifest.'),
    '#default_value' => variable_get('appcache_conditional_automatic', FALSE),
    '#states' => array(
      'visible' => array(
        ':input[name="appcache_conditional_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['appcache_conditional_custom'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="appcache_conditional_enabled"]' => array('checked' => TRUE),
      ),
      'invisible' => array(
        ':input[name="appcache_conditional_automatic"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['appcache_conditional_custom']['appcache_conditional_cache'] = array(
    '#type' => 'textarea',
    '#title' => t('CACHE section'),
    '#description' => t('Enter the resources to store in the application cache. Do not include the leading %base_url.', array('%base_url' => $GLOBALS['base_url'])),
    '#default_value' => implode("\r\n", variable_get('appcache_conditional_cache', array())),
  );

  $form['appcache_conditional_custom']['appcache_conditional_network'] = array(
    '#type' => 'textarea',
    '#title' => t('NETWORK section'),
    '#description' => t('Enter the resources to never store in the application cache. Do not include the leading %base_url.', array('%base_url' => $GLOBALS['base_url'])),
    '#default_value' => implode("\r\n", variable_get('appcache_conditional_network', array())),
  );

  $form['appcache_conditional_custom']['appcache_conditional_fallback'] = array(
    '#type' => 'textarea',
    '#title' => t('FALLBACK section'),
    '#description' => t('Enter the fallback paths to use if a resource is not found in the application cache. Do not include the leading %base_url.', array('%base_url' => $GLOBALS['base_url'])),
    '#default_value' => implode("\r\n", variable_get('appcache_conditional_fallback', array())),
  );

  $form['appcache_conditional_custom']['appcache_conditional_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('List of the pages'),
    '#description' => t('List the pages in which the manifest have to be shown. Do not include the leading %base_url. Put each page on a separate line. You can use the * character (asterisk) and any other pattern of the PHP fnmatch function.', array('%base_url' => $GLOBALS['base_url'])),
    '#default_value' => implode("\r\n", variable_get('appcache_conditional_pages', array())),
  );

  $form['appcache_conditional_custom']['appcache_conditional_disable_cachebuster'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable cachebuster to make offline application cache always up to date.'),
    '#default_value' => variable_get('appcache_conditional_disable_cachebuster', FALSE),
  );

  $form['#submit'] = array('appcache_conditional_settings_submit', 'appcache_conditional_cache_clear');

  return system_settings_form($form);
}

/**
 * FAPI submit callback to save appcache sections as arrays.
 */
function appcache_conditional_settings_submit($form, &$form_state) {
  $sections = array('cache', 'network', 'fallback', 'pages');

  foreach ($sections as $section) {
    $form_state['values']['appcache_conditional_' . $section] = explode("\r\n", $form_state['values']['appcache_conditional_' . $section]);
  }
}

